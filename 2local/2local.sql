-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2019 at 10:16 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2local`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `blog_image` varchar(300) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `tags` text NOT NULL,
  `date_posted` varchar(32) NOT NULL,
  `slug` text NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `user_id`, `title`, `blog_image`, `category_id`, `description`, `tags`, `date_posted`, `slug`, `views`) VALUES
(1, 1, 'A test blog post', 'blog5.jpg', 1, '<p>2Local stands for global sustainability and prosperity through making local and sustainable purchases accessible for everyone, by using a smart design of a digital currency for consumers and connected companies, based on innovative use of the blockchain technology.</p>\r\n\r\n<p>2Local is an initiative of creative entrepreneurs who love to come up with innovative solutions and want to apply them on a non-profit basis to expel poverty and hunger from the world and solve environmental and climate problems.</p>\r\n\r\n<p><br />\r\nFuture prospect<br />\r\nWe dream of a better future and try to get it done. This is a future without inequality and injustice, without malnutrition, and with sustainable use of natural resources. We want to give every individual a place in the global economy. A healthy future for our economy will be more local, less hectic and happier. We will consume less and differently in the future, not a fixation on economic growth, but ensure sustainable development and prosperity and not strive for more, but for better.</p>\r\n', '', '02-08-2019', 'a-test-blog-post', 0),
(2, 1, 'test blog 2', 'blog4.jpg', 2, '<p>We dream of a better future and try to get it done. This is a future without inequality and injustice, without malnutrition, and with sustainable use of natural resources. We want to give every individual a place in the global economy. A healthy future for our economy will be more local, less hectic and happier. We will consume less and differently in the future, not a fixation on economic growth, but ensure sustainable development and prosperity and not strive for more, but for better.</p>\r\n\r\n<p>We are not looking for an utopian economy, but an economy that takes into account all dimensions of man/woman: the social, the ecological and the economic. Like many scientific and policy reports recommend, we look for solutions in a local approach. With the help of modern cryptocurrencies, based on the Blockchain technology, we want to make local and sustainable products generally accessible, so that prosperity can be achieved for everyone!</p>\r\n', '', '02-08-2019', 'test-blog-2', 0),
(3, 1, 'Tets blog 3', 'blog3.jpg', 1, '<p>2Local stands for global sustainability and prosperity through making local and sustainable purchases accessible for everyone, by using a smart design of a digital currency for consumers and connected companies, based on innovative use of the blockchain technology.</p>\r\n\r\n<p>2Local is an initiative of creative entrepreneurs who love to come up with innovative solutions and want to apply them on a non-profit basis to expel poverty and hunger from the world and solve environmental and climate problems.</p>\r\n\r\n<p><br />\r\nFuture prospect<br />\r\nWe dream of a better future and try to get it done. This is a future without inequality and injustice, without malnutrition, and with sustainable use of natural resources. We want to give every individual a place in the global economy. A healthy future for our economy will be more local, less hectic and happier. We will consume less and differently in the future, not a fixation on economic growth, but ensure sustainable development and prosperity and not strive for more, but for better.</p>\r\n', '', '02-08-2019', 'tets-blog-3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `slug`) VALUES
(1, 'Sustainability', 0, 'sustainability'),
(2, 'Prosperity', 0, 'prosperity'),
(3, 'Food', 0, 'food'),
(4, 'People', 0, 'people');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `date_posted` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_view`
--

CREATE TABLE `post_view` (
  `post_id` int(11) NOT NULL,
  `user_ip` varchar(32) NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_view`
--

INSERT INTO `post_view` (`post_id`, `user_ip`, `views`) VALUES
(1, '1', 1),
(1, '1', 1),
(2, '2', 1),
(2, '2', 1),
(1, '1', 1),
(1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `name`, `type`) VALUES
(1, 'admin', 'c93ccd78b2076528346216b3b2f701e6', 'test@domainname.com', 'Usman Khan', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
