<?php 

function Verify_session() {
	$CI = &get_instance();

	$user_session_id = $CI->session->userdata('is_logged_id');

	if($user_session_id != true) {
		redirect('home');
	}
}