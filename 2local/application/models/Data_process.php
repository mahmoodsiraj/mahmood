<?php

class Data_process extends CI_model {
	public function dataUpdate($table_name, $data, $dataCondition) {
		$this->db->where($dataCondition);
		$this->db->update($table_name, $data);
	}
}