<?php

class Verify_User extends CI_Model {
	
	public function can_login($data) {
		$checkUser = $this->db->get_where('users', $data);

		if($checkUser->num_rows() > 0) {
			
			$dataSess = array(
				'uid' 		   => $checkUser->row()->id,
				'username' 	   => $checkUser->row()->username,
				'is_logged_id' => 1
			);

			$this->session->set_userdata($dataSess);

			return true;
		} else {
			return false;
		}
	}
}