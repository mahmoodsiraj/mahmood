<?php 

class Get_data extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function getAllUsers() {
		$query 		 = "SELECT users.*, account.userID, account.balance FROM users INNER JOIN account ON account.userID = users.id ORDER BY id DESC";
		$userRecords = $this->db->query($query);

		return $userRecords->result(); 
	}

	public function getSingleRecord($table_name, $dataCondition) {
		$dataGet = $this->db->get_where($table_name, $dataCondition);

		return $dataGet->result();
	}

	public function getAllRecord($table_name) {
		$dataGet = $this->db->get($table_name);

		return $dataGet->result();	
	}

	public function getUsers($condition = '') {
		$sql = $this->db->select('users.id as userID, users.username, users.dateCreated, users.verify_email, users.status, users.type, user_details.*');
		$sql = $this->db->join('user_details', 'users.id = user_details.user_id');

		if(!empty($condition)) {
			$sql = $this->db->where($condition);
		}
		$sql = $this->db->get('users');

		return $sql->result();
	}

	public function getBlogs() {
		$getData = $this->db->select('blogs.*, categories.name AS category');
		$getData = $this->db->join('categories', 'categories.id = blogs.category_id');
		$getData = $this->db->get('blogs');

		return $getData->result();
	}

	public function getBlogData($slug = null) {
		$sql = "SELECT blogs.*, users.name AS author, categories.name, categories.slug AS categorySlug FROM blogs INNER JOIN users ON users.id = blogs.user_id INNER JOIN categories ON categories.id = blogs.category_id";
		
		if($slug != null) {
			$sql .= " WHERE blogs.slug = '".$slug."'";
		}

		$getData = $this->db->query($sql);
		
		return $getData->result();
	}

	public function popularPosts() {
		$sql  = "SELECT * FROM blogs ORDER BY views DESC LIMIT 3";
		$data = $this->db->query($sql);

		return $data->result();
	}
}