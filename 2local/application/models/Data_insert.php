<?php 

class Data_insert extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function insertRecord($table_name, $data) {
		$this->db->insert($table_name, $data);
		$id = $this->db->insert_id();

		return $id;
	}
}
