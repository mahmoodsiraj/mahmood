<?php 

class Admin extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('admin/login');
	}

	public function verify() {
		$dataSet = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'type'	   => 1
		);

		$loginCheck = $this->verify_user->can_login($dataSet);

		if($loginCheck == true) {
			redirect('admin/blog');
		} else {
			$this->session->set_flashdata('message', "<i class='fa fa-times-circle'></i> Invalid Username / Password");
			redirect('admin'); 
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('admin');
	}

	public function blog($method = null) {
		$this->load->view('admin/layouts/header');
		
		if($method == 'create') {
			$data['categories'] = $this->get_data->getAllRecord('categories');
			$this->load->view('admin/createBlog', $data);
		} else {
			$data['blogData'] = $this->get_data->getBlogs();
			$this->load->view('admin/blog', $data);
		}

		$this->load->view('admin/layouts/footer');
	}

	public function categories($method = null) {
		$this->load->view('admin/layouts/header');
		
		$data['categories'] = $this->get_data->getAllRecord('categories');
		
		$this->load->view('admin/category', $data);
		$this->load->view('admin/layouts/footer');
	}

	public function deleteRecord() {
		$method = $this->input->post('method');
		$id 	= $this->input->post('id');

		$this->db->where('id', $id);
		$this->db->delete($method);
	}

	public function create_category() {
		$category_title = $this->input->post('category');
		$slug 		    = $this->input->post('slug');

		if($slug == null) {
			$slug = preg_replace('~[^\pL\d]+~u', '-', $category_title);
		  	$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
		  	$slug = preg_replace('~[^-\w]+~', '', $slug);
		  	$slug = trim($slug, '-');
		  	$slug = preg_replace('~-+~', '-', $slug);
		  	$slug = strtolower($slug);

			if (empty($slug)) {
				$slug = 'n-a';
			}
		} 

		$dataInsert = array(
			'name'   => $category_title,
			'parent' => 0,
			'slug'   => $slug
		);

		if($this->input->post('id') == true) {
			$dataUpdate = array(
				'name' => $packageTitle,
				'slug' => $slug
			);

			if($icon_name != '') {
				$dataUpdate['icon'] = $icon_name;
			} 

			$this->db->where('id', $this->input->post('id'));
			$this->db->update('packages', $dataUpdate);
		} else {
			$this->db->insert('categories', $dataInsert);
		}

		$this->session->set_flashdata('message', "<i class='fa fa-check-circle'></i> Category created successfully.");
		redirect('admin/categories');
	}

	public function create_blog() {
		$title 	  	 = $this->input->post('blogTitle');
		$category 	 = $this->input->post('category');
		$description = $this->input->post('description');

		$directoryPath 	= getcwd() . '/assets/uploads/blog/';
		$blog_name 		= $_FILES['blogImage']['name'];
		$blog_tmp  		= $_FILES['blogImage']['tmp_name'];

		move_uploaded_file($blog_tmp, $directoryPath.$blog_name);
		
		$slug = preg_replace('~[^\pL\d]+~u', '-', $title);
	  	$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
	  	$slug = preg_replace('~[^-\w]+~', '', $slug);
	  	$slug = trim($slug, '-');
	  	$slug = preg_replace('~-+~', '-', $slug);
	  	$slug = strtolower($slug);

		if (empty($slug)) {
			$slug = 'n-a';
		}
	
		$dataInsert = array(
			'title'   	  => $title,
			'category_id' => $category,
			'description' => $description,
			'date_posted' => date('d-m-Y'),
			'slug'		  => $slug
		);

		if($this->input->post('id') == true) {
			$dataUpdate = array(
				'name' => $packageTitle,
				'slug' => $slug
			);

			if($blog_name != '') {
				$dataUpdate['icon'] = $blog_name;
			} 

			$this->db->where('id', $this->input->post('id'));
			$this->db->update('packages', $dataUpdate);
		} else {
			if($blog_name != '') {
				$dataInsert['blog_image'] = $blog_name;
			} 
			$this->db->insert('blogs', $dataInsert);
		}

		$this->session->set_flashdata('message', "<i class='fa fa-check-circle'></i> Blog created successfully.");
		redirect('admin/blog');
	}
}