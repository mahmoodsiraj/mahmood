<?php 

defined('BASEPATH') or die('No direct scripts Allowed');

class Home extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

    public function index() { 
        $data = array(
            'title' => '2Local'
        );

    	$this->load->view('includes/header', $data);
        $this->load->view('includes/header_layout', $data);
    	$this->load->view('home');
    	$this->load->view('includes/footer');
    }

    public function blog($category = null, $blog = null) {
        $data = array(
            'title'     => '2Local',
            'settings'  => 'style="background-image:url('.base_url().'assets/img/blogbanner.png); background-repeat:no-repeat; background-size:cover; height: 300px;"'
        );

        $this->load->view('includes/header', $data);
        
        function get_client_ip() {
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
            return $ipaddress;
        }

        $userIP = get_client_ip();

        $data['popularPosts'] = $this->get_data->popularPosts();
        
        if($category == null && $blog == null) {
            $data['blogsData']    = $this->get_data->getBlogData();
            $data['categoryData'] = $this->get_data->getAllRecord('categories');
            $this->load->view('blog', $data);
        } else {
            $data['blogData']     = $this->get_data->getBlogData($blog);

            $checkLog = $this->db->get_where('post_view', array('user_ip' => $userIP, 'post_id' => $data['blogData'][0]->id));

            if($checkLog->num_rows() == 0) {
                $views = $data['blogData'][0]->views + 1;

                $this->db->where('id', $data['blogData'][0]->id);
                $this->db->update('blogs', array('views' => $views));

                $this->db->insert('post_view', array('user_ip' => $userIP, 'post_id' => $data['blogData'][0]->id));
            }

            $data['categoryData'] = $this->get_data->getAllRecord('categories');
            $data['userComments'] = $this->get_data->getSingleRecord('comments', array(
                'post_id' => $data['blogData'][0]->id
            ));

            $this->load->view('blogDetails', $data);
        }

        $this->load->view('includes/footer');   
    }

    public function postComment() {
        $id = $this->input->post('postID');

        $dataInsert = array(
            'comment'     => $this->input->post('comment'),
            'name'        => $this->input->post('FullName'),
            'email'       => $this->input->post('email'),
            'post_id'     => $id,
            'comment_id'  => 0,
            'date_posted' => date('F d, Y')
        );

        $this->db->insert('comments', $dataInsert);
        
        $sql = $this->db->get_where('comments', array('post_id' => $id));

        $dataShow['dataComments'] = $dataInsert;
        $dataComment      = $this->load->view('includes/commentPost', $dataShow, true);
        $dataCommentCount = ($sql->num_rows() > 1) ? $sql->num_rows() . ' Comments' : $sql->num_rows() . ' Comment';

        echo json_encode(array('htmlData' => $dataComment, 'dataComments' => $dataCommentCount));
    }
}