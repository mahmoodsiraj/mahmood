<div class="container">
    <div class="row blog-row">
        <div class="col-sm-12 text-center">
            <img src="<?php echo base_url(); ?>assets/img/blogbg.png" alt="" class="blog-head" >
        </div>
    </div>
</div>


</header>
<div class="blog-navbar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="blog-nav">
                    <ul>
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-caret-down"></i>
                                All Subject
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <?php foreach($categoryData as $category): ?>    
                                    <a class="dropdown-item" href="<?php echo base_url() . 'blog/' . $category->slug; ?>"><?php echo $category->name; ?></a>
                                <?php endforeach; ?>
                                </div>
                            </div>
                        </li>
                        
                        <li>
                            <a href="#">SUSTAINABILTY</a>
                        </li>
                        
                        <li>
                            <a href="#">PROSPERITY</a>
                        </li>
                        
                        <li>
                            <a href="#">FOOD</a>
                        </li>
                        
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="SEARCH">
                                <i class="fas fa-search"></i>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!--  /row -->
    </div><!--  /container  -->
</div><!--  /blog-navbar  -->
<!-- /blog nav-->

<!--  blog -->
<div class="container blog_page">
<div class="row">
<div class="col-sm-8">
    <div class="main-post">
        <?php $commentsCount = $this->db->get_where('comments', array('post_id' => $blogData[0]->id)); ?>
        <div class="post-title">
            <h2><?php echo $blogData[0]->title; ?></h2>
            <span class="post-date"><?php echo date('F d, Y', strtotime($blogData[0]->date_posted)); ?></span>
            <span class="comment"><?php echo $commentsCount->num_rows(); ?>  comment</span>
        </div>

        <div class="post-image">
            <img src="<?php echo base_url(); ?>assets/uploads/blog/<?php echo $blogData[0]->blog_image; ?>" alt="blog5" class="">
        </div>

        <div class="post-body">
             <?php echo $blogData[0]->description; ?> 
        </div><!--  /post-body  -->

        <div class="post-footer">
            <div class="row">
                <div class="col-sm-6 left">
                    <div class="post-author">
                        <h3>By <?php echo $blogData[0]->author; ?> </h3>
                    </div><!--  /post-author  -->
                </div>
                
                <div class="col-sm-6 right">
                    <ul>
                        <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="twt"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" class="linked"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!--  /post-footer  -->
    </div><!--  /main-post  -->

    <div class="newsletter">
        <h2>Never miss a News letter </h2>
        <form>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Subcribe</button>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
            </div>
        </form>
    </div>

    <br />
    <br />
    <br />

    <!--  comment  -->
    <div class="main-comment">
        <div class="row cmt-row">
            <div class="col-sm-12">
                <div class="com-head">
                    <h2>Comments</h2>
                </div>
            
                <h3>Leave a comment below</h3>
                <p>Thank you for leaving comments. Share you wisdom and please choose a star rating, too.</p>

                <form method="POST" class="postComment" onsubmit="return false;" action="<?php echo base_url() . 'home/postComment'; ?>">
                    <div class="form-group">
                        <label>Comment</label>
                        <textarea class="form-control" name="comment" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" class="form-control" name="FullName" id="exampleFormControlInput1" placeholder="">
                    </div>
                
                    <div class="form-group">
                        <label>Email *</label>
                        <input type="email" class="form-control last-input" name="email" id="exampleFormControlInput1" placeholder="">
                    </div>
                    <input type="hidden" name="postID" value="<?php echo $blogData[0]->id; ?>">
                    <button type="submit" class="btn btn-primary">comment</button>
                </form>
            </div>
        </div><!--  /comment row -->

        <hr>

        <div class="userComments">
        <?php if(count($userComments) > 0) { ?>    
        <?php foreach($userComments as $comment): ?>    
        <!--  cmt 1 -->
        <div class="cmt">
            <div class="row">
                <div class="col-sm-12">
                    <div class="cmt-title">
                        <h3><?php echo $comment->name; ?></h3>
                        <span><?php echo $comment->date_posted; ?></span>
                    </div>

                    <div class="cmt-body">
                        <div class="cmt-comment">
                            <p><?php echo $comment->comment; ?></p>
                        </div>
                        
                        <div class="cmt-reply">
                            <div class="cmt-reply-title">
                                <h3>Reply</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--  /cmt 1 -->

        <hr>
        <?php endforeach; ?>
        <?php } else { ?>
            <h4>No Comments Posted </h4>
        <?php } ?>
        </div>
    </div><!--  /main-comment  -->  
    <!--  /comment  -->

<!-- join-us -->
<!-- <div class="join-us">
<div class="row">
<div class="col-sm-8">

<div class="join-title">
<h2>Join Us Now</h2>
</div>
<form>
<div class="form-group">
<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Your Email">
<button type="submit" class="btn btn-primary">Subscribe Us</button>
</div>

</form>
</div>
<div class="col-sm-4">
<div class="join-us-social">
<ul>
<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#"><i class="fab fa-twitter"></i></a></li>
<li><a href="#"><i class="fab fa-youtube"></i></a></li>
<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>
</div> -->
<!-- / join-us -->





</div><!--  /blog col-sm-8  -->

<div class="col-sm-4">
<div class="blog-author-img">
<img src="<?php echo base_url(); ?>assets/img/bigteam1.png" class="img-circle img-responsive" alt="">
</div>
<div class="blog-author-name">
<h3>Harry Donkers</h3>
<p>Hello, I am a researcher and writer. I write about ...</p>
</div>

<div class="social-icons">
<ul>
<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#"><i class="fab fa-twitter"></i></a></li>
<li><a href="#"><i class="fab fa-youtube"></i></a></li>
<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
<!--  /social-icons  -->

<div class="getconect">
<h2>get connected...</h2>

<form>
<div class="form-group">
<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Your Email">
</div>
<button type="submit" class="btn btn-primary">Subscribe Us</button>

</form>
<div class="clear"></div>

</div>
<!--  /getconect  -->


    <div class="pop-article">
        <h2>Popular Articles</h2>

        <?php foreach($popularPosts as $posts): ?>
            <div class="art1">
                <p><?php echo $posts->title; ?></p>
                <img src="<?php echo base_url(); ?>assets/uploads/blog/<?php echo $posts->blog_image; ?>" alt="" class="img-responsive">
            </div>
        <?php endforeach; ?> 
    </div>
<!--  /pop-article  -->


</div>
<!--  /blog col-sm-4  -->
</div>
</div> 

<!--  blog END--> 

                  <!-- join-us -->
                  <div class="join-us">
                   <div class="container">
                    <div class="row">
                      <div class="col-sm-8">
                              <div class="row">
                      <div class="col-sm-8">

                        <div class="join-title">
                          <h2>Join Us Now</h2>
                        </div>
                        <form>
                         <div class="form-group">
                           <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Your Email">
                            <button type="submit" class="btn btn-primary">Subscribe Us</button>
                         </div>

                        </form>
                      </div>
                      <div class="col-sm-4">
                        <div class="join-us-social">
                          <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                          </ul>
                        </div>
                      </div>
                              </div>
                      </div>
                      <div class="col-sm-4"></div>        
                    </div>          
                   </div>
                  </div>
                  <!-- / join-us -->


     