        <section class="sec2">
        <div class="animebox">
          <img class="icon icon7" src="<?php echo base_url() . 'assets/img/'; ?>green-circle.png" alt="">
          <img class="icon icon8" src="<?php echo base_url() . 'assets/img/'; ?>red-triangle.png" alt="">
          <img class="icon icon24" src="<?php echo base_url() . 'assets/img/'; ?>u.png" alt="">
        </div>
            <div class="container">
                 <div class="row">
                  <div class="col-sm-12">
                    <div class="sec-heading">
                        <h2>2local Info</h2>
                    </div>
                  </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-7">
                      <div class="slider-nav"></div>
                     <div class="single-item">
                         <div>
                         <div class="sub-head">
                         <h2>Token Facts</h2>
                        </div>
                     <p>Reveneu model, long term yield strategy & professional distribution management associated with the L2L utility token.</p>
                     <p>Pre Sale starts September 1st, 70% bonus   |   Main Sale starts September 19th, 30% bonus</p>
                     <div class="chart text-center">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>chart1.png" alt="chart">
                     </div>

                     <div class="row chart-list">
                         <div class="col-sm-4">
                             <ul>
                                 <li class="one">BlockChain Development</li>
                                 <li class="two">business Development</li>
                                 <li class="three">Reearch sustainability</li>
                             </ul>
                         </div>
                         <div class="col-sm-4">
                                <ul>
                                 <li class="four">Algorithm Development</li>
                                 <li class="five">Product Management</li>
                                 <li class="six">Research Economy</li>
                             </ul>
                         </div>
                         <div class="col-sm-4">
                                <ul>
                                 <li class="seven">Web & App Develope</li>
                                 <li class="eight">Marketing</li>
                                 <li class="nine">Overhead</li>
                             </ul>
                         </div>
                     </div>

                     </div>
                     
                         

                         <div>
                         <div class="sub-head">
                         <h2>Token Facts</h2>
                     </div>
                     <p>Reveneu model, long term yield strategy & professional distribution management associated with the L2L utility token.</p>
                     <p>Pre Sale starts September 1st, 70% bonus   |   Main Sale starts September 19th, 30% bonus</p>
                     <div class="chart text-center">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>chart2.png" alt="chart">
                     </div>

                     <div class="row chart-list">
                         <div class="col-sm-4">
                             <ul>
                                 <li class="one">BlockChain Development</li>
                                 <li class="two">business Development</li>
                                 <li class="three">Reearch sustainability</li>
                             </ul>
                         </div>
                         <div class="col-sm-4">
                                <ul>
                                 <li class="four">Algorithm Development</li>
                                 <li class="five">Product Management</li>
                                 <li class="six">Research Economy</li>
                             </ul>
                         </div>
                         <div class="col-sm-4">
                                <ul>
                                 <li class="seven">Web & App Develope</li>
                                 <li class="eight">Marketing</li>
                                 <li class="nine">Overhead</li>
                             </ul>
                         </div>
                     </div>
                     </div>
                         

                         <div><div class="sub-head">
                         <h2>Token Facts</h2>
                     </div>
                     <p>Reveneu model, long term yield strategy & professional distribution management associated with the L2L utility token.</p>
                     <p>Pre Sale starts September 1st, 70% bonus   |   Main Sale starts September 19th, 30% bonus</p>
                     <div class="chart text-center">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>chart1.png" alt="chart">
                     </div>

                     <div class="row chart-list">
                         <div class="col-sm-4">
                             <ul>
                                 <li class="one">BlockChain Development</li>
                                 <li class="two">business Development</li>
                                 <li class="three">Reearch sustainability</li>
                             </ul>
                         </div>
                         <div class="col-sm-4">
                                <ul>
                                 <li class="four">Algorithm Development</li>
                                 <li class="five">Product Management</li>
                                 <li class="six">Research Economy</li>
                             </ul>
                         </div>
                         <div class="col-sm-4">
                                <ul>
                                 <li class="seven">Web & App Develope</li>
                                 <li class="eight">Marketing</li>
                                 <li class="nine">Overhead</li>
                             </ul>
                         </div>
                     </div>
                     </div>
                     </div>
                     <!-- /slider -->

                     <div class="row circle-half-width">
                      <div class="col-sm-3">
                        <div class="buttonHolder" style="width:100px;">
                            <div class="hButtonFlipper">
                                <!-- Standard button -->
                              <a type="button" class="btn button1">
                    <figure class="ball">
                      <div class="ulCont">
                    <ul>
                      <li>
                           Start Main <br> Sale September <br>
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                      <li>Symbol
                      <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                      <li>Start Main <br>Sale September 
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                      <li>Symbol
                        <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button1  --> 
                              <a type="button" class="btn button2">
                    <figure class="ball">
                      <div class="ulCont">
                    <ul>
                      <li>Symbol
                      <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                      <li>
                           Start Main <br> Sale September <br>
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                      <li>Symbol
                        <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                      <li>Start Main <br>Sale September 
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button2  -->
                            </div><!--  /hButtonFlipper  -->
                        </div><!--  /buttonHolder  -->
                       </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>Main Sale<br>
                        Bonus
                      <h4>30%</h4>
                    </li>
                    <li>
                    </li>
                    <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                    </li>
                    <li></li>
                    <li>Main Sale<br>
                        Bonus
                      <h4>30%</h4>
                    </li>
                    <li>
                    </li>
                    <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                      <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                      </li>
                      <li>
                      </li>
                      <li>Main Sale<br>
                        Bonus
                       <h4>30%</h4>
                      </li>
                      <li>
                      </li>
                      <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                      </li>
                      <li>Main Sale<br>
                        Bonus
                       <h4>30%</h4>
                      </li>
                      <li>
                      </li>
                    </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                    <li></li>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                    <li></li>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                    <li></li>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                    <li></li>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                   </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    <li></li>
                    <li>
                      ICO 
                      <h4>Tokens</h4>
                    </li>
                    <li></li>
                    <li>
                    <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    <li></li>
                    <li>
                      ICO 
                      <h4>Tokens</h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                      <li>
                        ICO 
                        <h4>Tokens</h4>
                      </li>
                      <li></li>
                      <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    <li></li>
                    <li>
                      ICO 
                      <h4>Tokens</h4>
                    </li>
                    <li></li>
                      <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                    </div> <!-- /row circle -->

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="row circle-half-width">
                      <div class="col-sm-3">
                        <div class="buttonHolder" style="width:100px;">
                            <div class="hButtonFlipper">
                                <!-- Standard button -->
                              <a type="button" class="btn button1">
                    <figure class="ball">
                      <div class="ulCont">
                    <ul>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                      <li>
                      </li>
                      <li>
                      <h4 style="margin-top:0px;">Min</h4>
                      All Sale
                      <br>
                      2<i class="fas fa-euro-sign"></i> 5oct
                      </li>
                      <li>
                      </li>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                      <li></li>
                      <li>
                      <h4 style="margin-top:0px;">Min</h4>
                      All Sale
                      <br>
                      2<i class="fas fa-euro-sign"></i> 5oct
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button1  --> 
                              <a type="button" class="btn button2">
                    <figure class="ball">
                      <div class="ulCont">
                       <ul>
                        <li>
                         <h4 style="margin-top:0px;">Min</h4>
                         All Sale
                         <br>
                         2<i class="fas fa-euro-sign"></i> 5oct
                        </li>
                        <li></li>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                      <li>
                      </li>
                      <li>
                      <h4 style="margin-top:0px;">Min</h4>
                      All Sale
                      <br>
                      2<i class="fas fa-euro-sign"></i> 5oct
                      </li>
                      <li>
                      </li>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button2  -->
                            </div><!--  /hButtonFlipper  -->
                        </div><!--  /buttonHolder  -->
                       </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                    <li>
                    </li>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                    <li></li>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                    <li>
                    </li>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                    <li></li>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                    <li>
                    </li>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                    <li></li>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                  </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                    <li></li>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                    <li></li>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                    <li></li>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                    <li></li>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                    <li></li>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                    <li></li>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                    <li></li>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                    <li></li>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                    <li></li>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                  </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                    </div> <!-- /row circle -->




                     
                     
                          
                      </div> 
                      <div class="col-sm-5">
                          <div class="coins">
                              <img src="<?php echo base_url() . 'assets/img/'; ?>coin(ver2).png" alt="coin" class="img-responsive">
                              <!--<div class="coin1">
                                  <img src="<?php echo base_url() . 'assets/img/'; ?>newcoin1.png" class="img-responsive" alt="coin1" >
                              </div>
                              <div class="coin2">
                                  <img src="<?php echo base_url() . 'assets/img/'; ?>newcoin2.png" class="img-responsive" alt="coin1" >
                              </div>-->
                          </div>

                          <div class="frm">
                              <div class="frm-header">
                                  <img src="<?php echo base_url() . 'assets/img/'; ?>form-header.png" alt="">
                              </div>
                              <div class="frm-body">
                                   <label>AMOUNT EURO</label>
                                  <div class="input-group">
                                     <input type="text" class="form-control"  aria-describedby="basic-addon1">
                                      <img src="<?php echo base_url() . 'assets/img/'; ?>euro.png" alt="">
                                 </div>
                                 <label>AMOUNT TOKENS</label>
                                 <div class="input-group">
                                      <input type="text" class="form-control" placeholder="L2L" aria-describedby="basic-addon1">
                                      <img src="<?php echo base_url() . 'assets/img/'; ?>2.png" alt="">
                                 </div>

                                 <a href="" class="btn">Buy Now</a>
                              </div>
                          </div>

                      </div>
                  </div>

                 

                     <div class="row circle-full-width">
                       <div class="col-sm-3">
                        <div class="buttonHolder" style="width:100px;">
                            <div class="hButtonFlipper">
                                <!-- Standard button -->
                              <a type="button" class="btn button1">
                    <figure class="ball">
                      <div class="ulCont">
                    <ul>
                      <li>
                           Start Main <br> Sale September <br>
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                      <li>Symbol
                      <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                      <li>Start Main <br>Sale September 
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                      <li>Symbol
                        <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button1  --> 
                              <a type="button" class="btn button2">
                    <figure class="ball">
                      <div class="ulCont">
                    <ul>
                      <li>Symbol
                      <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                      <li>
                           Start Main <br> Sale September <br>
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                      <li>Symbol
                        <h4>L2L</h4>
                      </li>
                      <li>
                      </li>
                      <li>Start Main <br>Sale September 
                        <h4>19th</h4>
                      </li>
                      <li>
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button2  -->
                            </div><!--  /hButtonFlipper  -->
                        </div><!--  /buttonHolder  -->
                       </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>Main Sale<br>
                        Bonus
                      <h4>30%</h4>
                    </li>
                    <li>
                    </li>
                    <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                    </li>
                    <li></li>
                    <li>Main Sale<br>
                        Bonus
                      <h4>30%</h4>
                    </li>
                    <li>
                    </li>
                    <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                      <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                      </li>
                      <li>
                      </li>
                      <li>Main Sale<br>
                        Bonus
                       <h4>30%</h4>
                      </li>
                      <li>
                      </li>
                      <li>1L2L
                      <h4>0.12 <i class="fas fa-euro-sign"></i></h4>
                      </li>
                      <li>Main Sale<br>
                        Bonus
                       <h4>30%</h4>
                      </li>
                      <li>
                      </li>
                    </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                    <li></li>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                    <li></li>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                    <li></li>
                    <li>
                      Total
                      <br>
                      Tokens
                      <h4>21B</h4> 
                    </li>
                    <li></li>
                    <li>
                      Type
                      <h4>Utility</h4>                        
                    </li>
                   </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    <li></li>
                    <li>
                      ICO 
                      <h4>Tokens</h4>
                    </li>
                    <li></li>
                    <li>
                    <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    <li></li>
                    <li>
                      ICO 
                      <h4>Tokens</h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                      <li>
                        ICO 
                        <h4>Tokens</h4>
                      </li>
                      <li></li>
                      <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    <li></li>
                    <li>
                      ICO 
                      <h4>Tokens</h4>
                    </li>
                    <li></li>
                      <li>
                      Algorithm
                      <br>
                      Backed
                      <h4>Stable</h4>
                    </li>
                    </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->
                     </div>



                     <div class="row circle-full-width">
                       <div class="col-sm-3">
                        <div class="buttonHolder" style="width:100px;">
                            <div class="hButtonFlipper">
                                <!-- Standard button -->
                              <a type="button" class="btn button1">
                    <figure class="ball">
                      <div class="ulCont">
                    <ul>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                      <li>
                      </li>
                      <li>
                      <h4 style="margin-top:0px;">Min</h4>
                      All Sale
                      <br>
                      2<i class="fas fa-euro-sign"></i> 5oct
                      </li>
                      <li>
                      </li>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                      <li></li>
                      <li>
                      <h4 style="margin-top:0px;">Min</h4>
                      All Sale
                      <br>
                      2<i class="fas fa-euro-sign"></i> 5oct
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button1  --> 
                              <a type="button" class="btn button2">
                    <figure class="ball">
                      <div class="ulCont">
                       <ul>
                        <li>
                         <h4 style="margin-top:0px;">Min</h4>
                         All Sale
                         <br>
                         2<i class="fas fa-euro-sign"></i> 5oct
                        </li>
                        <li></li>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                      <li>
                      </li>
                      <li>
                      <h4 style="margin-top:0px;">Min</h4>
                      All Sale
                      <br>
                      2<i class="fas fa-euro-sign"></i> 5oct
                      </li>
                      <li>
                      </li>
                      <li>
                        <h4 style="margin-top:0px;">KYC</h4>
                        Above
                        <br>
                        <i class="fas fa-euro-sign"></i> 1000 Month
                      </li>
                    </ul>
                      </div>
                    </figure>
                              </a><!--  /button2  -->
                            </div><!--  /hButtonFlipper  -->
                        </div><!--  /buttonHolder  -->
                       </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                    <li>
                    </li>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                    <li></li>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                    <li>
                    </li>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                    <li></li>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                    <li>
                    </li>
                    <li>
                      Pre Sale
                      <br>
                      Bonus
                      <h4>70%</h4>
                    </li>
                    <li></li>
                    <li>Fiat BTC<br>
                      <h4>ETH XLM</h4>
                    </li>
                  </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                    <li></li>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                    <li></li>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                    <li>
                    </li>
                    <li>
                      Stellar
                      <h4>Based</h4> 
                    </li>
                    <li></li>
                    <li>
                      Start
                      <br>
                      Pre Sale
                      <h4 style="font-size:20px;">September 1st</h4>                        
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->

                      <div class="col-sm-3">
              <div class="buttonHolder" style="width:100px;">
               <div class="hButtonFlipper">
                 <!-- Standard button -->
                 <a type="button" class="btn button1">
                    <figure class="ball">
                  <div class="ulCont">
                  <ul>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                    <li></li>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                    <li></li>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                    <li></li>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                  </ul>
                   
                  </div>
                    </figure>
                 </a><!--  /button1  --> 


                 <a type="button" class="btn button2">
                    <figure class="ball">
                  <div class="ulCont">
                   
                    <ul>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                    <li></li>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                    <li></li>
                    <li>
                      Hard Cap
                      <h4>15M <i class="fas fa-euro-sign" style="font-size:20px;"></i></h4>
                    </li>
                    <li></li>
                    <li>
                      Regulation 
                      <h4>AFM</h4>
                      <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt="flag" class="dutch-flag">
                    </li>
                  </ul>
                  </div>
                    </figure>
                 </a><!--  /button2  -->
                </div><!--  /hButtonFlipper  -->
              </div><!--  /buttonHolder  -->
                      </div><!--  /col-sm-3  -->
                     </div>


                
            </div>
        </section>

       

       <section class="sec3">
           <div class="container">
           <div class="animebox">
          <img class="icon icon9" src="<?php echo base_url() . 'assets/img/'; ?>green-circle.png" alt="">
          <img class="icon icon10" src="<?php echo base_url() . 'assets/img/'; ?>red-triangle.png" alt="">
          </div>
           <div class="row">
            <div class="col-sm-12">
                    <div class="sec-heading">
                        <h2>Connected Companies</h2>
                    </div>
                  </div>
                  </div>
               <div class="row ">
                <div class="slider-nav1"></div>
                   

               <div class="multiple-items">

                   <div class="col-sm-4 slider_div">
                       <div class="logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>company-logo1.png" alt="">
                       </div>
                       <p>Fabian9 is modern 3D-Art with principles of all times, that invokes more awareness for a better world</p>
                   </div>

                     <div class="col-sm-4 slider_div" style="transform: translate3d(0px, -50px, 0px); !important">
                       <div class="logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>company-log2.png" alt="">
                       </div>
                       <p>SmartReading creating wisdom in<br> a smart way, wisdom from which<br> a sustainable world evolves</p>
                   </div>

                     <div class="col-sm-4 slider_div">
                       <div class="logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>company-logo3.png" alt="">
                       </div>
                       <p>Attraction Dating matches people on their mindset in their living area, people in a relationship are happier, healtier and will live longer</p>
                   </div>

                    <div class="col-sm-4 slider_div">
                       <div class="logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>company-log2.png" alt="">
                       </div>
                       <p>SmartReading creating wisdom in<br> a smart way, wisdom from which<br> a sustainable world evolves</p>
                   </div>

                     <div class="col-sm-4 slider_div" style="transform: translate3d(0px, -50px, 0px); !important">
                       <div class="logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>company-logo3.png" alt="">
                       </div>
                       <p>Attraction Dating matches people on their mindset in their living area, people in a relationship are happier, healtier and will live longer</p>
                   </div>

                     <div class="col-sm-4 slider_div">
                       <div class="logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>company-logo1.png" alt="">
                       </div>
                       <p>Fabian9 is modern 3D-Art with principles of all times, that invokes more awareness for a better world</p>
                   </div>
               </div>
               </div><!-- -->
            </div>
       </section>

       <section class="sec4">
           <div class="container">
           <div class="animebox">
             <img class="icon icon11" src="<?php echo base_url() . 'assets/img/'; ?>green-circle.png" alt="">
             <img class="icon icon12" src="<?php echo base_url() . 'assets/img/'; ?>red-triangle.png" alt="">
             <img class="icon icon13" src="<?php echo base_url() . 'assets/img/'; ?>moon.png" alt="">
             <img class="icon icon14" src="<?php echo base_url() . 'assets/img/'; ?>dotted.png" alt="">
           </div>
               <div class="row">
                    <div class="col-sm-12">
                    <div class="sec-heading">
                        <h2>Our Teams</h2>
                    </div>
                  </div>
               </div>
               
                <div class="row">
                  <div class="col-sm-12">
                      <ul class="nav nav-tabs">
                       <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Core Team</a></li>
                       <li role="presentation"><a href="#tab2" data-toggle="tab">Developers</a></li>
                       <li role="presentation"><a href="#tab3" data-toggle="tab">Ambassadors</a></li>
                       <li role="presentation"><a href="#tab4" data-toggle="tab">Contributors</a></li>
                     </ul>
                  </div>
                  <div class="clear"></div>
                </div><!-- /tabs row -->

             

              <div class="row">
              	<div class="col-sm-12">
              	<div class="tab-content">
                <div class="tab-pane active" id="tab1">
              		
  						<div class="single-item1">

  						<div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>antony-big.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Antony Chang (Ing)</h2>
                           <h4>CEO & Founder</h4>
                           <a href="https://www.linkedin.com/in/changantony"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 0 -->

   						 <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam1.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Harry Donkers (PhD)</h2>
                           <h4>CSO & Co-Founder</h4>
                           <a href="https://www.linkedin.com/in/harry-donkers-64778613/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 1 -->
                       
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam5.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Henk de Leeuw</h2>
                           <h4>CFO  & Co-Founder</h4>
                           <a href="https://www.linkedin.com/in/henk-de-leeuw-6719a254/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 5 -->
                      


                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>mitchel-big.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Mitchel</h2>
                           <h4>Web Developer</h4>
                           <a href="https://www.linkedin.com/in/mitchel-dankers-920869173/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 3 -->



                        <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam8.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Aura Kuipers</h2>
                           <h4>CCO & Co-Founder</h4>
                           <a href="https://www.linkedin.com/in/aura-kuipers/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 8 -->


                       


                        <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam2.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Rutger in den Bosch (Ing)</h2>
                           <h4>CTO & Co-Founder</h4>
                           <a href="https://www.linkedin.com/in/rutger-in-den-bosch-a234ba22/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 2 -->


                        <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam9.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Irma van Utrecht</h2>
                           <h4>Virtual Assistant</h4>
                           <a href="https://www.linkedin.com/in/irma-van-utrecht-085660173/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 9 -->


                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam7.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Stratos Kokkinakias (LLM)</h2>
                           <h4>CLO & Co-Founder</h4>
                           <a href="https://www.linkedin.com/in/stratos-k-05173214b/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div><!-- 7 -->


                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>amir-big.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Amir Naghavi (Ing)</h2>
                           <h4>Android Developer</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div><!-- 4 -->


                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigteam6.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Gilbert van den Broek</h2>
                           <h4>ICO Advisor</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div><!-- 6 -->


                      


                        
  						</div>
  						<div class="slider-nav1a"></div>
  						
  						</div><!---->
                         
                         <div class="tab-pane" id="tab2">
                         	<div class="single-item2">
   						 <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigtahira.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Tahira Kulsoom</h2>
                           <h4>Business Development</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigrafay.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Raffay Ansari</h2>
                           <h4>Blockchain Developer</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bighamza.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Hamza Yasin</h2>
                           <h4>Blockchain Developer</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigusman.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Usman Manzoor</h2>
                           <h4>Web Integration</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigshan.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Shan Arshad</h2>
                           <h4>Backend Developer</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                        <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigsohail.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Sohail Akram</h2>
                           <h4>Quality Assurance</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                        <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>biganjum.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Anjum Shazad</h2>
                           <h4>Head Of Development</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigsadia.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Sadia Bruce</h2>
                           <h4>Business Manager</h4>
                           <a href="https://www.linkedin.com/in/sadia-bruce-782471ab/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div>
                       
                      
                        
  						</div>
  						<div class="slider-nav2a"></div>

                         </div><!-- /tab2-->

                         <div class="tab-pane" id="tab3">
                         <div class="single-item3">
   						 <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bighalmat.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Halmat Rashid</h2>
                           <h4>Iraq Ambassador</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>

                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bighalmat.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Halmat Rashid</h2>
                           <h4>Iraq Ambassador</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       
                       
                       
                       
                      
                       
                       
                       
                       
  						</div>
  						<div class="slider-nav3a"></div>

                         </div><!-- /tab3-->

                         <div class="tab-pane" id="tab4">
                         	<div class="single-item4">
   						 <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigmuzammalashfaq.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Muzammal Ashfaq</h2>
                           <h4>Explainer Video</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>
                       
                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigfabian.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Fabian Schlosser</h2>
                           <h4>2local Logo & Coin Designer</h4>
                           <img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt="">
                         </div>
                        </div> 
                       </div>

                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigmahmood.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Syed Mahmood Siraj</h2>
                           <h4>Web Designer & Front End Developer</h4>
                           <a href="https://www.linkedin.com/in/mahmoodsiraj/"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div>

                       <div>
                          <div class="row">
                         <div class="col-sm-8">
                         <img src="<?php echo base_url() . 'assets/img/'; ?>bigshoaib.png" alt="">
                         </div>
                         <div class="col-sm-4 founder-info">
                           <h2>Shoaib Khokhar</h2>
                           <h4>IT Consultant</h4>
                           <a href="https://www.linkedin.com/in/m-shoaib-khokhar"><img src="<?php echo base_url() . 'assets/img/'; ?>in.png" alt=""></a>
                         </div>
                        </div> 
                       </div>
                      
                       
                       
                        
                       
                      
                        
  						</div>
  						<div class="slider-nav4a"></div>
                         </div>



  						</div><!-- /tab-content-->
              	</div>

              	
              </div>































           </div>
       </section>

       <section class="sec5">
       <div class="wave"><div class="wave-pic"></div></div>
         <div class="container">
         <div class="animebox">
            <img class="icon icon15" src="<?php echo base_url() . 'assets/img/'; ?>moon.png" alt="">
         </div>
               <div class="row">
                    <div class="col-sm-12">
                    <div class="sec-heading">
                        <h2>Watch Video</h2>
                    </div>
                  </div>
               </div>
               
           <div class="row">
             <div class="col-sm-7">

                    
                     <div class="tab-content">
                      <div class="tab-pane active" id="tab5">
                       
                       <iframe width="560" height="315" src="https://www.youtube.com/embed/TLbeNFZi25U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      </div>

                      

                        

                      <div class="tab-pane" id="tab6"><iframe width="560" height="315" src="https://www.youtube.com/embed/dGYpHxKraeE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

                      <div class="tab-pane" id="tab7"><iframe width="560" height="315" src="https://www.youtube.com/embed/E4Hr34ucvSU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

                     </div>
                   
                     
                       <ul class="nav nav-tabs">
                       <li role="presentation" class="active"><a href="#tab5" data-toggle="tab">Explainer ENG</a></li>
                       <li role="presentation"><a href="#tab6" data-toggle="tab">Explainer NL</a></li>
                       <li role="presentation"><a href="#tab7" data-toggle="tab">Promo Video</a></li>
                     </ul><!-- nav tabs-->
                        
                      
                      <ul class="nav nav-tabs page-tabs">
                      <li role="presentation"><a href="#" data-toggle="tab">More</a></li>
                       <li role="presentation" class="active"><a href="#tab5" data-toggle="tab">1</a></li>
                       <li role="presentation"><a href="#tab6" data-toggle="tab">2</a></li>
                       <li role="presentation"><a href="#tab7" data-toggle="tab">3</a></li>
                     </ul><!-- page-tabs-->
                    

                      

                     
                     

                   


               
             </div>
             <div class="col-sm-5">
               <div class="old-man">
                 <img src="<?php echo base_url() . 'assets/img/'; ?>old-man.png" alt="old-man">
               </div>
             </div>
           </div>
         </div>
       </section>

       <section class="sec6">
         <div class="container">
         <div class="animebox">
           <img class="icon icon16" src="<?php echo base_url() . 'assets/img/'; ?>green-circle.png" alt="">
           <img class="icon icon17" src="<?php echo base_url() . 'assets/img/'; ?>red-triangle.png" alt="">
           <img class="icon icon18" src="<?php echo base_url() . 'assets/img/'; ?>moon.png" alt="">
           <img class="icon icon19" src="<?php echo base_url() . 'assets/img/'; ?>moon.png" alt="">
           <img class="icon icon20" src="<?php echo base_url() . 'assets/img/'; ?>moon.png" alt="">
           <img class="icon icon21" src="<?php echo base_url() . 'assets/img/'; ?>green-circle.png" alt="">
           <img class="icon icon25" src="<?php echo base_url() . 'assets/img/'; ?>u.png" alt="">
           <img class="icon icon28" src="<?php echo base_url() . 'assets/img/'; ?>location.png" alt="">

         </div>
              <div class="row">
                    <div class="col-sm-12">
                    <div class="sec-heading">
                        <h2>Roadmap</h2>
                    </div>
                  </div>
               </div>


           <div class="row">
             <div class="col-sm-12">
               <div class="roadmap text-center">
                 <img src="<?php echo base_url() . 'assets/img/'; ?>roadmap.png" alt="roadmap">
               </div>
             </div>
           </div>
         </div>
       </section>
       <!-- /road map-->

       <section class="sec7" id="getintouch">
       <div class="wave"><div class="wave-pic"></div></div>
         <div class="container">
         <div class="animebox">
           <img class="icon icon22" src="<?php echo base_url() . 'assets/img/'; ?>green-circle.png" alt="">
           <img class="icon icon23" src="<?php echo base_url() . 'assets/img/'; ?>red-triangle.png" alt="">
           </div>
         <div class="row">
                    <div class="col-sm-12">
                    <div class="sec-heading">
                        <h2>Get In Touch</h2>
                    </div>
                  </div>
               </div>


           <div class="row">
           <div class="col-sm-6">
              <form>
              <div class="row">

              <div class="col-sm-12">

              <!--<input type="radio" name="gender" value="male">asdasd <br>
              <input type="radio" name="gender" value="female">asdasd<br>
              <input type="radio" name="gender" value="female">asdasd<br>
              -->

              <ul>
  <li>
    <input type="radio" name="options" id="option1" value="1" />
    <label for="option1">Question or Idea</label>
  </li>
  <li>
    <input type="radio" name="options" id="option2" value="2" />
    <label for="option2">Become an Ambassador</label>
  </li>
  <li>
    <input type="radio" name="options" id="option3" value="3" />
    <label for="option3">Become a Connected Company</label>
  </li>
</ul>


              </div> 

              <div class="col-sm-6">
              <label>Name</label>
               <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter Your Full Name" aria-describedby="basic-addon1">
                <img src="<?php echo base_url() . 'assets/img/'; ?>user.png" alt="">
               </div>
              </div>

              <div class="col-sm-6">
              <label>Email</label>
              <div class="input-group">
              <input type="email" class="form-control" placeholder="Enter Your Email Address" aria-describedby="basic-addon2">
              <img src="<?php echo base_url() . 'assets/img/'; ?>email.png" alt="">
             </div>
             </div>

             <div class="col-sm-6">
             <label>Phone</label>
             <div class="input-group">
              <input type="text" class="form-control" placeholder="Enter Your Phone Number" aria-describedby="basic-addon2">
              <img src="<?php echo base_url() . 'assets/img/'; ?>phone.png" alt="">
             </div>
             </div>

             <div class="col-sm-6">
             <label>Website</label>
             <div class="input-group">
              <input type="text" class="form-control" placeholder="Enter Your Website" aria-describedby="basic-addon2">
              <img src="<?php echo base_url() . 'assets/img/'; ?>world.png" alt="">
             </div>
             </div>

             <div class="col-sm-12">
             <div class="input-group">
               <textarea placeholder="Write Something!"></textarea>
             </div>
             </div>

             <a class="btn sec7-btn" href="#">Send</a>
             </div>
             </form>


           </div>

           <div class="col-sm-6">
           <div class="woman">
             <img src="<?php echo base_url() . 'assets/img/'; ?>woman.png" class="">
           </div>  
           </div>             
           </div>
         </div>
       </section>

