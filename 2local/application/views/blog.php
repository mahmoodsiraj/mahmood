<div class="container">
    <div class="row blog-row">
        <div class="col-sm-12 text-center">
            <img src="<?php echo base_url(); ?>assets/img/blogbg.png" alt="" class="blog-head" >
        </div>
    </div>
</div>
</header>

<div class="blog-navbar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="blog-nav">
                    <ul>
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-caret-down"></i>
                                All Subject
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <?php foreach($categoryData as $category): ?>    
                                    <a class="dropdown-item" href="<?php echo base_url() . 'blog/' . $category->slug; ?>"><?php echo $category->name; ?></a>
                                <?php endforeach; ?>
                                </div>
                            </div>
                        </li>
                        <?php $i = 1; ?>
                        <?php foreach($categoryData as $category): ?> 
                        <?php if($i <= 3) { ?> 
                        <li>
                            <a href="<?php echo base_url() . 'blog/' . $category->slug; ?>">
                              <?php echo $category->name; ?>
                            </a>
                        </li>
                      <?php } ?>
                        <?php $i++; ?>
                        <?php endforeach; ?>

                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="SEARCH">
                                <i class="fas fa-search"></i>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!--  /row -->
    </div><!--  /container  -->
</div><!--  /blog-navbar  -->
<!-- /blog nav-->

 <!--  blog -->
<div class="container blog_page">
    <div class="row">
        <div class="col-sm-8">
        <?php foreach($blogsData as $blog): ?>    
        <?php $commentsCount = $this->db->get_where('comments', array('post_id' => $blog->id)); ?>    
            <div class="main-post">
                <div class="post-title">
                    <h2>
                        <a href="<?php echo base_url() . 'blog/'.$blog->categorySlug . '/' . $blog->slug; ?>"><?php echo $blog->title; ?></a>
                    </h2>
                    <span class="post-date">
                        <?php echo date('F d, Y', strtotime($blog->date_posted)); ?>
                    </span>
                    <span class="comment"><?php echo $commentsCount->num_rows(); ?> comment</span>
                </div>

                <div class="post-image">
                    <img src="<?php echo base_url(); ?>assets/uploads/blog/<?php echo $blog->blog_image; ?>" alt="blog5" class="">
                </div>

                <div class="post-body">
                <?php 
                  $content = strip_tags($blog->description); 
                  $length  = strlen($content);
                  if($length > 502) {
                ?>
                  <?php echo substr($content, 0, 500); ?>
                  <?php } else { ?>
                    <?php echo $blog->description; ?> 
                  <?php } ?>                  
                </div><!--  /post-body  -->

                <div class="post-footer">
                    <div class="row">
                        <div class="col-sm-6 left">
                        <?php if($length > 502) { ?>  
                            <p>
                                <a href="<?php echo base_url() . 'blog/'.$blog->categorySlug . '/' . $blog->slug; ?>">Continue to the article >>></a>
                            </p>
                        <?php } ?>    
                        </div>
                    
                        <div class="col-sm-6 right">
                            <ul>
                                <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twt"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="linked"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!--  /post-footer  -->

                <!--  tags -->
                <div class="tags">
                   <div class="row">
                     <div class="col-sm-12">
                       <div class="tag-title">
                         <h2>Category</h2>
                       </div>
                     </div>
                   </div>
                   
                  <div class="row">
                    <div class="col-sm-12">
                      <ul>
                        <li>
                           <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck1" >
                             <label class="form-check-label" for="exampleCheck1">sustainability</label>
                           </div>
                        </li>
                        <li>
                           <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck2" >
                             <label class="form-check-label" for="exampleCheck2">prosperity</label>
                           </div>
                           </li>
                           <li>
                            <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck3" >
                             <label class="form-check-label" for="exampleCheck3">Food</label>
                            </div>
                           </li>
                           <li>
                             <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck4" >
                             <label class="form-check-label" for="exampleCheck4">people</label>
                            </div>
                           </li>
                           <li>
                             <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck5" >
                             <label class="form-check-label" for="exampleCheck5">planet</label>
                            </div>
                           </li>
                      </ul>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <ul>
                        <li>
                           <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck6" >
                             <label class="form-check-label" for="exampleCheck6">local 2 local</label>
                           </div>
                        </li>
                        <li>
                           <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck7" >
                             <label class="form-check-label" for="exampleCheck7">economy</label>
                           </div>
                           </li>
                           <li>
                            <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck8" >
                             <label class="form-check-label" for="exampleCheck8">wealth</label>
                            </div>
                           </li>
                           <li>
                             <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck9" >
                             <label class="form-check-label" for="exampleCheck9">innovation</label>
                            </div>
                           </li>
                           <li>
                             <div class="form-group form-check">
                             <input type="checkbox" class="form-check-input" id="exampleCheck10" >
                             <label class="form-check-label" for="exampleCheck10">FARMING</label>
                            </div>
                           </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!--  /tags --> 
            </div><!--  /main-post  -->
        <?php endforeach; ?>    
        </div><!--  /blog col-sm-8  -->

           <div class="col-sm-4">
              <div class="blog-author-img">
                 <img src="<?php echo base_url() . 'assets/'; ?>img/bigteam1.png" class="img-circle img-responsive" alt="">
              </div>
              <div class="blog-author-name">
                <h3>Harry Donkers</h3>
                <p>Hello, I am a researcher and writer. I write about ...</p>
              </div>
              
                <div class="social-icons">
                          <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                          </ul>
                </div>
                <!--  /social-icons  -->

                <div class="getconect">
                  <h2>get connected...</h2>

                  <form>
                         <div class="form-group">
                           <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address . . .">
                         </div>
                         <button type="submit" class="btn btn-primary">SUBSCRIBE</button>

                  </form>
                  <div class="clear"></div>

                </div>
                <!--  /getconect  -->

                <div class="pop-article">
                  <h2>Popular Articles</h2>

                  <?php foreach($popularPosts as $posts): ?>
                   <div class="art1">
                     <p><?php echo $posts->title; ?></p>
                     <img src="<?php echo base_url(); ?>assets/uploads/blog/<?php echo $posts->blog_image; ?>" alt="" class="img-responsive">
                   </div>
                  <?php endforeach; ?> 
                </div>
                <!--  /pop-article  -->
           </div>
           <!--  /blog col-sm-4  -->
         </div>
       </div> 
       <!--  blog END--> 

                  


       


