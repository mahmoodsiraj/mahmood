<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <div class="panel-body">
          <form method="POST" action="<?php echo base_url() . 'admin/updateSettings'; ?>" enctype="multipart/form-data">
            <div class="form-group col-md-6">
              <label>Site Logo</label>
              <div class="img-logo">
                <img src="<?php echo base_url() . 'assets/images/'.$dataSettings[0]->slide_image; ?>" />
              </div>
              <input type='file' name="logoImage" class="form-control" />
            </div>

            <div class="form-group">
                <label>Site Title</label>
                <input type='text' name="site_title" class="form-control" value="{{ $dataSettings[0]->siteTitle }}" />
              </div>

              <div class="form-group">
                <label>Footer Text</label>
                <input type='text' name="footer_text" class="form-control" value="{{ $dataSettings[0]->footerText }}" />
              </div>

            <div class="form-group col-md-12">
                <label>Social Icons</label>
                <textarea type='text' name="description" id="content" class="form-control" row="6"></textarea>
              </div>

              <script>
                  CKEDITOR.replace( 'content', {
                      filebrowserUploadUrl: '<?php echo base_url() . 'assets/'; ?>ckeditor/plugins/filebrowser/uploader/upload.php'
                  });
              </script>
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>

          <div class="clearfix"></div>
          <br />
          <br />
        </div>
      </div>