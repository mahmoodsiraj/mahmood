 <div id="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <div class="panel-body">
        <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Blog

          <a href="<?php echo base_url() . 'admin/blog/create'; ?>" class="btn btn-primary align-right">Add Blog</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Category</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
               <?php foreach($blogData as $blog): ?>
                <tr>
                  <td><?php echo $blog->id; ?></td>
                  <td><img src="<?php echo base_url() . 'assets/uploads/blog/' . $blog->blog_image ; ?>" /></td>
                  <td><?php echo $blog->title; ?></td>
                  <td><?php echo $blog->category; ?></td>
                  <td>
                    <a href="{{ url( 'slides/edit/' . $slide->id ) }}">Edit</a> | 
                    <a href="#">Delete</a>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
        <br />
      </div>
    </div>
    <!-- /.container-fluid -->
