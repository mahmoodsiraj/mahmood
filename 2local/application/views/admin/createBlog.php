<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <div class="panel-body">
          <form method="POST" action="<?php echo base_url() . 'admin/create_blog'; ?>" enctype="multipart/form-data">
            <div class="form-group col-md-6">
              <label>Blog Title</label>
              <input type='text' name="blogTitle" class="form-control" />
            </div>

            <div class="form-group col-md-6">
              <label>Category</label>
              <select name="category" class="form-control">
                <option value="0">Select Category</option>
              <?php foreach ($categories as $category) { ?>
                <option value="<?php echo $category->id; ?>">
                  <?php echo $category->name; ?>
                </option>
              <?php } ?>  
              </select>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-12">
              <label>Featured Image</label>
              <!--<div class="img-logo">
                <img src="{{ asset('images/backgrounds/'.$slideData[0]->slide_image) }}" />
              </div>-->
              <input type='file' name="blogImage" class="form-control" />
            </div>

            <div class="form-group col-md-12">
                <label>Blog Description</label>
                <textarea type='text' name="description" id="content" class="form-control" row="6"></textarea>
              </div>

              <script>
                  CKEDITOR.replace( 'content', {
                      filebrowserUploadUrl: '<?php echo base_url() . 'assets/'; ?>ckeditor/plugins/filebrowser/uploader/upload.php'
                  });
              </script>
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>

          <div class="clearfix"></div>
          <br />
          <br />
        </div>
      </div>