@extends('admin.layouts.app')

@section('content')
    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <div class="panel-body">
          @if(session('success'))
            <div class="alert alert-success" role="alert">
              <strong>Well done!</strong> {{session('success')}}
            </div>
          @endif
          <form class="col-md-6" method="POST" action="{{ url('upload_logo') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label>Site Logo</label>
              <div class="img-logo">
                <img src="{{ asset('images/'.$dataSettings[0]->LogoImage) }}" />
              </div>
              <input type='file' name="logoImage" class="form-control" />
            </div>

            <button type="submit" class="btn btn-primary ">Update Logo</button>
          </form>

          <div class="clearfix"></div>

          <br />
          <br />
          <form class="col-md-6" method="POST" action="{{ url('update_settings') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Site Title</label>
                <input type='text' name="site_title" class="form-control" value="{{ $dataSettings[0]->siteTitle }}" />
              </div>

              <div class="form-group">
                <label>Footer Text</label>
                <input type='text' name="footer_text" class="form-control" value="{{ $dataSettings[0]->footerText }}" />
              </div>

            <button type="submit" class="btn btn-primary ">Update Settings</button>
          </form>

          <br />
          <br />
        </div>
      </div>
      <!-- /.container-fluid -->
@endsection