 <div id="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <div class="panel-body">
        <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Blog

          <a href="<?php echo base_url() . 'admin/blog/create'; ?>" class="btn btn-primary align-right">Add Blog</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
               @foreach($dataSlides as $slide)
                <tr>
                  <td>{{ $slide->id }}</td>
                  <td><img src="{{ asset('images/backgrounds/' . $slide->slide_image ) }}" /></td>
                  <td>{{ $slide->slide_text }}</td>
                  <td>
                    <a href="{{ url( 'slides/edit/' . $slide->id ) }}">Edit</a> | 
                    <a href="#">Delete</a>
                  </td>
                </tr>
              @endforeach  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
        <br />
      </div>
    </div>
    <!-- /.container-fluid -->
