<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <div class="panel-body">
            <?php if($this->session->flashdata('message') == true) { ?>
                <div class="message">
                    <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>
          <form method="POST" class="col-md-5" action="<?php echo base_url() . 'admin/create_category'; ?>">
            <div class="form-group">
              <label>Category Title</label>
              <input type='text' name="category" class="form-control" />
            </div>

            <div class="clearfix"></div>

            <div class="form-group ">
              <label>Slug</label>
              <input type='text' name="slug" class="form-control" />
            </div>

            <button type="submit" class="btn btn-primary ">Submit</button>
           
          </form>

            <div class="col-md-7">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>category</th>
                                    <th>Slug</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach($categories as $category): ?>
                                    <tr>
                                        <td><?php echo $category->id; ?></td>
                                        <td><?php echo $category->name ; ?> </td>
                                        <td><?php echo $category->slug; ?></td>
                                        <td>
                                            <a href="<?php echo $category->id; ?>">Edit</a> | 
                                            <a href="#">Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

          <div class="clearfix"></div>
          <br />
          <br />
        </div>
      </div>