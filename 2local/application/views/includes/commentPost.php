<div class="cmt">
    <div class="row">
        <div class="col-sm-12">
            <div class="cmt-title">
                <h3><?php echo $dataComments['name']; ?></h3>
                <span><?php echo $dataComments['date_posted']; ?></span>
            </div>

            <div class="cmt-body">
                <div class="cmt-comment">
                    <p><?php echo $dataComments['comment']; ?></p>
                </div>
                
                <div class="cmt-reply">
                    <div class="cmt-reply-title">
                        <h3>Reply</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--  /cmt 1 -->

<hr>