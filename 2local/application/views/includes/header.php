<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <!-- IE 10 Metro tile icon (Metro equivalent of apple-touch-icon) -->
        <meta name="msapplication-TileColor" content="#393939">
        <meta name="msapplication-TileImage" content="<?php echo base_url() . 'assets/img/'; ?>fav/favicon-144.png">
        <!-- IE 11 Tile for Windows 8.1 Start Screen -->
        <meta name="application-name" content="YOUR APP NAME">
        <meta name="msapplication-tooltip" content="APP NAME">
        <meta name="msapplication-config" content="<?php echo base_url() . 'assets/img/'; ?>fav/browserconfig.xml">
        <!-- General favicon -->
        <link rel='shortcut icon' type='image/x-icon' href="<?php echo base_url() . 'assets/img/'; ?>fav/favicon.ico" />
        <!-- Touch icon for iOS 2.0+ and Android 2.1+ -->
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url() . 'assets/img/'; ?>fav/apple-icon.png" />
        <link rel="apple-touch-icon" href="<?php echo base_url() . 'assets/img/'; ?>fav/apple-icon.png">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,900i&display=swap" rel="stylesheet">
        <!-- font family-->
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- font awesome -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/'; ?>css/slick.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/'; ?>css/keyframe.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/'; ?>css/bee3D.min.css">
       
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/main.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/responsive.css">
        <script src="<?php echo base_url() . 'assets/'; ?>js/vendor/modernizr-3.5.0.min.js"></script>
    </head>
    <body>

    <header <?php if(isset($settings)) { echo $settings;} ?>>
        <nav>
            <div class="container">
                <div class="navbar ">
                        <div class="social-link">
                            <ul class="nav navbar-nav">
                                <li><a href="https://www.facebook.com/2local-2297961593816000"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/2local1"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/19002320/"><i class="fab fa-linkedin-in"></i></a></li>
                                <li>
                                    <!--<i class="fab fa-google"></i>-->
                                    <!-- Single button -->
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <!--<span class="caret"></span>-->
                                     </button>
                                     <!--<ul class="dropdown-menu">
                                        <li><a href="#">ENGLISH <img src="<?php echo base_url() . 'assets/img/'; ?>usflag.png" alt=""></a></li>
                                        <li><a href="#">DUTCH  <img src="<?php echo base_url() . 'assets/img/'; ?>flag.png" alt=""></a></li>
                                     </ul>-->
                                    </div>
                                </li>
                                <li><a href="https://t.me/ICO_2local"><i class="fas fa-paper-plane"></i></a></li>
                            </ul>
                        </div>

                    <div class="navbar-brand">
                        <a href=""><img src="<?php echo base_url() . 'assets/img/'; ?>logo.png"></a>
                    </div>

                    <button class="navbar-toggle" data-toggle="collapse" data-target="#mk">
                        <i class="fas fa-bars"></i>
                    </button>


                <div class="collapse navbar-collapse navbar-right" id="mk">
                    <ul class="nav navbar-nav ">
                        <li><a href="">Team</a></li>
                          <li><a href="<?php echo base_url() . 'blog'; ?>">blog</a></li>
                          <li><a href="">Contact</a></li>
                          <li><a href="">Login</a></li>
                    </ul>
                </div>
                        
                  </div>

                </div>
            </nav>


           