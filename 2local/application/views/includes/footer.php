
 <footer>
           <div class="footer-top">
               <div class="container">
                   <div class="row">
                       <div class="col-sm-2 ftr-logo">
                           <img src="<?php echo base_url() . 'assets/img/'; ?>ftr-logo.png" alt="">
                       </div>
                       <div class="col-sm-3">
                           <div class="sec-heading">
                           <h3>About Us</h3>
                           </div>
                           <ul>
                           <li><a href="#">Discover 2local</a></li>
                           <li><a href="index.html">Road Map</a></li>
                           <li><a href="index.html">Companies</a></li>
                           <li><a href="index.html">Core Team</a></li>
                           <li><a href="index.html">Developers</a></li>
                           <li><a href="index.html">Contributors</a></li>
                           </ul>
                       </div>
                       <div class="col-sm-3">
                           <div class="sec-heading">
                           <h3>2local Token</h3>
                           </div>
                           <ul>
                           <li><a href="https://2local.io/docs/2local-whitepaper-en.pdf" target="_blank">Whitepaper ENG</a></li>
                           <li><a href="https://2local.io/docs/Privacy_Policy.pdf" target="_blank">Privacy Policy</a></li>
                           <li><a href="https://2local.io/docs/2local-whitepaper-nl.pdf" target="_blank">Whitepaper NL</a></li>
                           <li><a href="https://2local.io/docs/Privacy_Verklaring.pdf" target="_blank">Privacy Verklaring</a></li>
                           <li><a href="https://2local.io/docs/Terms_and_Conditions.pdf" target="_blank">Why 2Local</a></li>
                           <li><a href="https://2local.io/docs/Algemene_Voorwaarden.pdf" target="_blank">Token Allocation</a></li>
                           <li><a href="#" target="_blank">Use Of Funds</a></li>
                           </ul>
                       </div>
                       <div class="col-sm-2">
                           <div class="sec-heading">
                           <h3>Contact Us</h3>
                           </div>
                           <ul>
                           <li><a href="mailto:info@2local.io">info@2local.io</a></li>
                           <li><a href="https://2local.io/SignUp">Sign up</a></li>
                           <li><a href="index.html#getintouch" onclick="ambassador()" class="amb">Ambassador</a></li>
                           <li><a href="index.html#getintouch">Leave a Message</a></li>
                           <li><a href="index.html#getintouch">Company</a></li>
                           <li><a href="blog.html">Blog</a></li>
                           </ul>
                       </div>
                       <div class="col-sm-2 social-link-ftr">
                            <div class="sec-heading">
                           <h3>Follow Us</h3>
                           </div>
                                 <ul>
                                   <li><a href="https://www.facebook.com/2local-2297961593816000"><i class="fab fa-facebook-f"></i></a></li>
                                   <li><a href="https://twitter.com/2local1"><i class="fab fa-twitter"></i></a></li>
                                   <li><a href="https://www.linkedin.com/company/19002320/"><i class="fab fa-linkedin-in"></i></a></li>
                                   <li><a href="https://t.me/ICO_2local"><i class="fas fa-paper-plane"></i></a></li>
                                  </ul>
                        </div>
                      
                       
                   </div>
               </div>
           </div>
           <div class="footer-bottom">
             <div class="container">
              <p>2019 Copyright 2local<sup> <i class="far fa-registered"></i></sup> . All Rights Reserved</p>
             </div>
           </div>
       </footer>
       
     



        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


      <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script> 
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="<?php echo base_url() . 'assets'; ?>/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url() . 'assets'; ?>/js/plugins.js"></script>
        <script src="<?php echo base_url() . 'assets'; ?>/js/main.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script type="text/javascript" src="<?php echo base_url() . 'assets'; ?>/js/slick.js"></script>

        <script type="text/javascript">
        $('.single-item').slick({
        appendArrows: $(".slider-nav"),
        nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
        prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>'    
        });
        
        
        </script>
       

       <!-- <script type="text/javascript">
        $('.single-item4').slick({
        appendArrows: $(".slider-nav5"),
       // nextArrow: '<button class="right-arrow"><i class="fas fa-chevron-right"></i></button>',
       nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
        prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>'    
        });
        </script>

        <script type="text/javascript">
        $('.single-item3').slick({
        appendArrows: $(".slider-nav4"),
       // nextArrow: '<button class="right-arrow"><i class="fas fa-chevron-right"></i></button>',
       nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
        prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>'    
        });
        </script>

        <script type="text/javascript">
        	$('.single-item2').slick({
			appendArrows: $(".slider-nav3"),
			// nextArrow: '<button class="right-arrow"><i class="fas fa-chevron-right"></i></button>',
			nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
			prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>'    
			});
		</script>-->

        


         <script type="text/javascript">
        $('.single-item1').slick({
      dots: true,
      arrows:true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $(".slider-nav1a"),
      nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
	  prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>', 
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });
        </script>


       <script type="text/javascript">
        $('.single-item2').slick({
      dots: true,
      arrows:true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $(".slider-nav2a"),
      nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
	  prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>', 
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });
        </script>


 <script type="text/javascript">
        $('.single-item3').slick({
      dots: true,
      arrows:true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $(".slider-nav3a"),
      nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
	  prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>', 
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });
        </script>

        <script type="text/javascript">
        $('.single-item4').slick({
      dots: true,
      arrows:true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $(".slider-nav4a"),
      nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
	  prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></button>',   
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });
        </script>

       
        

        

        <script type="text/javascript">

        $('.multiple-items').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        appendArrows: $(".slider-nav1"),
        nextArrow: '<button class="right-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-right.png" alt=""></button>',
        prevArrow: '<button class="left-arrow"><img src="<?php echo base_url() . 'assets/img/'; ?>arrow-left.png" alt=""></i></button>',
        responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
        });</script>

        <script type="text/javascript">
        //      function ambassador(){
        //        $('.sec7 #option1').addClass("checked"); 
        //        $('.sec7 input[type="radio"]').setAttribute("checked"); 
              
        //     }
            

                
               

         </script>

         <script type="text/javascript">
            $( document ).ready(function() {  $( ".slider-nav .slick-arrow" ).click(function() {     $( ".hButtonFlipper" ).toggleClass( "flipped" );   }); });
         </script>


        
        
       
     
     
     
       
    </body>
</html>
