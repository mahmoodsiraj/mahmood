$(document).on('submit', '.postComment', function() {
	var formData = new FormData(this);

    $.ajax({
        url : $(this).attr('action'),
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function( response ) {
            if($('.userComments').children().hasClass('cmt')) {
                $('.userComments').prepend( response.htmlData );
            } else {
                $('.userComments').html( response.htmlData );
            }

            $('.post-title .comment').html( response.dataComments );

            $('.postComment')[0].reset();
        }
    });
});